
clear all
format long
load fig2C.data;
g=fig2C;

scale=10; % for stress in KPa 
for i=1:31
  for j=1:41 
    stress(i,j)=(g(41*(i-1)+j,3))/scale;
  end  
end

load fig2CShell.data;
gsh=fig2CShell;
scalesh=0.01; % for stress in KPa 

for i=1:31
  for j=1:41
    stresssh(i,j)=(gsh(41*(i-1)+j,3))/scalesh;
  end  
end
imagesc((stresssh-stress)')
ylabel('Young modulus (KPa)');
xlabel('Poisson ratio')
hcb=colorbar;
ylabel(hcb,'S_{shell} - S_{TRBS} (KPa)')

