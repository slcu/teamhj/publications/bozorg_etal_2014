AA=figure 

load fig2aP2.data;
load fig2aP4.data;
load fig2aP2shell.data;
load fig2aP4shell.data;

A=fig2aP2;
B=fig2aP4;
C=fig2aP2shell;
D=fig2aP4shell;

def=A(:,1);

for i=1:size(def)
def(i)=100*(def(i)-0.707134)/0.707134;
end

sp2=A(:,5); % area ratio
sp4=B(:,5);

for i=1:size(def)
sp2(i)=sp2(i);
sp4(i)=sp4(i);
end


plot(def,sp2,'r')
hold on
plot(def,sp4,'g')

defsh=C(:,1);

for i=1:size(defsh)
defsh(i)=100*(defsh(i)-0.707134)/(2*0.707134);
end

sp2sh=C(:,8);
sp4sh=D(:,8);
for i=1:size(defsh)
sp2sh(i)=sp2sh(i);
sp4sh(i)=sp4sh(i);
end

plot(defsh,sp2sh,'+r')
hold on
plot(defsh,sp4sh,'xg')
xlim([0 65])
ylim([0.9 1.5])
xlabel('X deflection (percent)');
ylabel('area ratio')

legend('\nu=0.2 TRBS','\nu=0.4 TRBS','\nu=0.2 shell','\nu=0.4 shell')
